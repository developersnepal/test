<style>
.p_style a{
	color:#C00;
	text-decoration:none;}
	.p_style a:hover{
	color:#900;
	text-decoration:none;}
	</style>
     <h7>BIOGRAPHY</h7>
     
     <p class="p_style">Born on 10th Sept, 1981 in Morang, Biratnagar, Jiwan Luitel, is one of the most prolific and talented actors of current Nepali Film Industry. Having completed his graduation, he ventured into the world of fashion as a promising model announcing his big entrance with a coveted title of Mr. Nepal 2002. Thus, his career in movies began - being offered a role in an upcoming Nepali Movie following his title win. The years that ensued saw him establish as a man of natural talent, excelling in acting and handling his stardom with finesse and humility.</p>
     <p class="p_style">The seed of acting was showed early in his life. He always wanted to be an actor. The title win of Mr. Nepal paved his way to achieve his ambition. He started his career from a movie -Tirkha-, but it could not really quench his thirst of acting. He strived to be better, to be that person whom he had dreamed of ever since he was a small kid.</p>
     <p class="p_style"> 
Landing a role in -Sano Sansar- as a character artist launched him in a very new light in the Nepali Movie Industry. He was recognized widely for his talent. Following his next movie -Nasib Aafno-, he won the title of KTV Best Debutant Actor 2010. It marked his first step into the world of Nepali Movie Industry. Ever since, there has been no looking back; step-by-step he has climbed the ladder of success following that event to stand with us today as one of the renowned names of current era of actors.</p>
     
    